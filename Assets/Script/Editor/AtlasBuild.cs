﻿/** 
 *Author:  Sorpcboy   
 *Description: 图集打包 
*/
#if UNITY_EDITOR
using UnityEngine;
using System.IO;
using UnityEditor;
using System.Text;
using System.Diagnostics;

public class AtlasBuild : Editor
{
    [MenuItem("Tools/AtlasBuild", priority = 101)]
    public static void BuildTexturePacker()
    {
        //< 选择并设置TP命令行的参数和参数值：
        //< --trim-sprite-names  去除png等后缀
        //< --multipack 多图片打包开起，避免资源图太多，生成图集包含不完全，开起则会生成多张图集。
        //< --maxrects-heuristics macrect的算法  参数 Best ShortSideFit LongSideFit AreaFit BottomLeft ContactPoint
        //< --enable-rotation 开起旋转，计算rect时如果旋转将会使用更优的算法来处理，得到更小的图集
        //< --border-padding 精灵之间的间距
        //< --shape-padding 精灵形状填充
        //< --trim-mode Trim 删除透明像素，大下使用原始大小。 参数 None Trim Crop CropKeepPos Polygon
        //< --basic-sort-by Name  按名称排序
        //< --basic-order Ascending 升序
        //< --texture-format 纹理格式
        //< --data 输出纹理文件的信息数据路径 plist
        //< --sheet 输出图集路径 png
        //< --scale 1 缩放比例 主要用于低分辨率的机子多资源适配。
        //< --max-size 最大图片像素 一般我是用的2048，超过2048以前的有些android机型不支持。
        //< --size-constraints 结纹理进行大小格式化，AnySize 任何大小 POT 使用2次幂 WordAligned
        //< --replace 正则表达式，用于修改plist加载后的名称
        //< --pvr-quality PVRTC 纹理质量
        //< --force-squared 强制使用方形
        //< --etc1-quality ETC 纹理质量

        string commandText =" --sheet {0}.png --data {1}.tpsheet --format unity-texture2d --trim-mode None --pack-mode Best --algorithm MaxRects --max-size 2048 --size-constraints POT  --disable-rotation --scale 1 {2}";
        string inputPath = string.Format("{0}/ImageDir", Application.dataPath);//小图目录
        string[] imagePath = Directory.GetDirectories(inputPath);
        for (int i = 0; i < imagePath.Length; i++)
        {
            UnityEngine.Debug.Log(imagePath[i]);
            StringBuilder sb = new StringBuilder("");
            string[] fileName = Directory.GetFiles(imagePath[i]);
            for (int j = 0; j < fileName.Length; j++)
            {
                string extenstion = Path.GetExtension(fileName[j]);
                if (extenstion == ".png")
                {
                    sb.Append(fileName[j]);
                    sb.Append("  ");
                }
                UnityEngine.Debug.Log("fileName [j]:" + fileName[j]);
            }
            string name = Path.GetFileName(imagePath[i]);
            string outputName = string.Format("{0}/AtlasPacker/{1}/{2}", Application.dataPath, name ,name); //< 第一个Name作为文件路径， 第二个Name为图集名字
            //< 调用TP命令行工具，执行命令
            processCommand("D:\\Program Files (x86)\\CodeAndWeb\\TexturePacker\\bin\\TexturePacker.exe", string.Format(commandText, outputName, outputName, sb.ToString()));
        }
        AssetDatabase.Refresh();
    }

    private static void processCommand(string command, string argument)
    {
        ProcessStartInfo start = new ProcessStartInfo(command);
        start.Arguments = argument;
        start.CreateNoWindow = false;
        start.ErrorDialog = true;
        start.UseShellExecute = false;

        if (start.UseShellExecute)
        {
            start.RedirectStandardOutput = false;
            start.RedirectStandardError = false;
            start.RedirectStandardInput = false;
        }
        else
        {
            start.RedirectStandardOutput = true;
            start.RedirectStandardError = true;
            start.RedirectStandardInput = true;
            start.StandardOutputEncoding = System.Text.UTF8Encoding.UTF8;
            start.StandardErrorEncoding = System.Text.UTF8Encoding.UTF8;
        }

        Process p = Process.Start(start);
        if (!start.UseShellExecute)
        {
            UnityEngine.Debug.Log(p.StandardOutput.ReadToEnd());
            UnityEngine.Debug.Log(p.StandardError.ReadToEnd());
        }

        p.WaitForExit();
        p.Close();
    }
}
#endif